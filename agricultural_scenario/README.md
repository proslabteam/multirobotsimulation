# Multi-Robot application in an Agricultural Scenario

## Description
The proposed application scenario consists of two (or possibly more) tractors and a drone which cooperate in order to  identify  and remove weeds in a farmland, and thus increase the farm productivity. 
Both the drone and the tractors are equipped with a controller, enabling computations and communications, a battery, and a several sensors and actuators. Specifically, the drone has four propellers, a laser sensor for the obstacle avoidance, and a camera for the image recognition. While the tractors have wheels moved by engines, a rotating blade for cutting weeds, and the laser sensor.  
At the system start-up, the drone is the only robot that starts its behavior, it receives the boundaries of the field to inspect, and starts the exploration. During the overflight of the field, the drone use the camera sensor to recognize weeds and, when found, it sends to the tractors the coordinates.
This enacts the tractors which store the weed coordinates and send back to the drone their distance to the weed. 
Therefore the drone can elect the closest tractor and notify it. At this point, the tractor elected by the drone starts moving towards the field, avoiding possible obstacles. Once it reaches the weed it activates the blade, cuts the weed, and stops its process until it receives a new position from the drone.

## BPMN model

![BPMN model](./docs/bpmn_models/agriculture_bpmn_model.png)

### Call Activities
| Name | Model |
| :------------- |:-------------:|
| Detect | <img src="./docs/bpmn_models/detect.png" width="600" />|
| Explore | <img src="./docs/bpmn_models/explore.png" width="600" /> |
| Return to Base | <img src="./docs/bpmn_models/returnbase.png" width="600" /> |

## Directory description
    .
    ├── launch
    │   ├── drone_controller.launch.py      # Drone launch file
    │   ├── multi_launch.py                 # World and robots initialization
    │   ├── spawn_elements.py               # Executable for spawning robot description files into a Gazebo world
    │   ├── tractor_controller.launch.py    # Tractors launch file
    ├── models                              # Folders that contains useful the gazebo source files
    |   ├── ...
    ├── robot_description  
    │   ├── tractor.sdf                     # Ground robot description file
    |   ├── tello.sdf                       # Tello drone description file
    ├── src
    |   ├── drone
    |   |   ├── explore.py
    |   |   ├── flight_controller.py
    |   |   ├── return_to_base.py
    |   |   ├── weed_sub_process.py
    |   ├── tractor
    |   |   ├── cut_grass.py
    |   |   ├── tractor_controller.py
    |   ├── replace.py                      # Executable for the replacement of a placeholder in the robot description files
    ├── worlds
    │   ├── agriculture.world           # Configuration file of the gazebo world
    ├── CMakeLists.txt
    ├── package.xml
    └── README.md

