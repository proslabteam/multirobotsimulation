#!/usr/bin/env python3
import random
import os
from launch import LaunchDescription
from launch_ros.actions import Node
from datetime import datetime
from launch.actions import ExecuteProcess, DeclareLaunchArgument
from launch.substitutions.launch_configuration import LaunchConfiguration
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    tractor_ns_1 = 'tractor_1'
    tractor_ns_2 = 'tractor_2'
    tractor_ns_3 = 'tractor_3'

    drone_ns = 'drone_1'

    now = str(datetime.now()).replace(' ', '_')
    log_folder = 'logged_files/' + now
    log_all = log_folder + '_all'

    return LaunchDescription([
        Node(package='agriculture', executable='tractor_controller.py', output='screen',
             namespace=tractor_ns_1),

        Node(package='agriculture', executable='battery_controller.py', output='screen',
             # set a random battery percentage
             namespace=tractor_ns_1, arguments=[str(random.randint(30, 40))]),

        Node(package='agriculture', executable='tractor_controller.py', output='screen',
             namespace=tractor_ns_2),

        Node(package='agriculture', executable='battery_controller.py', output='screen',
             namespace=tractor_ns_2, arguments=[str(random.randint(50, 100))]),

        Node(package='agriculture', executable='tractor_controller.py', output='screen',
             namespace=tractor_ns_3),

        Node(package='agriculture', executable='battery_controller.py', output='screen',
             namespace=tractor_ns_3, arguments=[str(random.randint(50, 100))]),

        Node(package='agriculture', executable='flight_controller.py', output='screen',
             namespace=drone_ns),

        Node(package='agriculture', executable='weed_sub_process.py',
             output='screen', namespace=drone_ns),

        Node(package='agriculture', executable='battery_controller.py', output='screen',
             namespace=drone_ns, arguments=[str(random.randint(80, 100))]),


        #DeclareLaunchArgument('without_odom', default_value=['`ros2 topic list | grep -v -e "/*/odom" -e "tf" -e "/*/macro"`']),

        #ExecuteProcess(cmd=['ros2', 'bag', 'record', '-o', log_all,LaunchConfiguration('without_odom')], shell=True),

        #        ExecuteProcess(cmd=['ros2', 'bag', 'record', '-o',                            log_odom, (drone_ns+'/odom'),  (tractor_ns_1+'/odom'), (tractor_ns_2+'/odom'), (tractor_ns_3+'/odom')], shell=True),

    ])
