import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from datetime import datetime


def generate_launch_description():

    pkg_agriculture = get_package_share_directory('agriculture')

    now = str(datetime.now()).replace(' ', '_')
    log_folder = 'logged_files/' + now
    log_macro = log_folder + '_macro'
    log = log_folder + '_logs'

    tractor_ns_1 = 'tractor_1'
    tractor_ns_2 = 'tractor_2'
    tractor_ns_3 = 'tractor_3'

    drone_ns = 'drone_1'

    tractor_1 = os.path.join(pkg_agriculture, 'robot_description',
                             str(tractor_ns_1 + '.sdf'))

    tractor_2 = os.path.join(pkg_agriculture, 'robot_description',
                             str(tractor_ns_2 + '.sdf'))

    tractor_3 = os.path.join(pkg_agriculture, 'robot_description',
                             str(tractor_ns_3 + '.sdf'))

    drone_1 = os.path.join(
        pkg_agriculture, 'robot_description', str(drone_ns + '.sdf'))

    world_path = os.path.join(pkg_agriculture, 'worlds', 'agriculture.world')

    return LaunchDescription([
        # Launch Gazebo, loading the world
        ExecuteProcess(cmd=[
            # 'gazebo',
            'gzserver',
            '--verbose',
            '-s', 'libgazebo_ros_init.so',  # Publish /clock
            '-s', 'libgazebo_ros_factory.so',  # Provide gazebo_ros::Node
            # '-r', '--record_path', log_macro,  # record the simulation in a precise folder
            world_path
        ], output='screen'),

        # Spawn robots.urdf
        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[tractor_1, '0', '1', '0', '0', tractor_ns_1]),

        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[tractor_2, '0', '2', '0', '0', tractor_ns_2]),

        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[tractor_3, '0', '3', '0', '0', tractor_ns_3]),

        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[drone_1, '0', '5', '0', '0', drone_ns]),


        ExecuteProcess(cmd=['ros2', 'bag', 'record', '-o',
                            log, (drone_ns+'/macro'),  (tractor_ns_1 + \
                                                        '/macro'), (tractor_ns_2+'/macro'), (tractor_ns_3+'/macro'),
                            (drone_ns+'/odom'),  (tractor_ns_1+'/odom'), (tractor_ns_2+'/odom'), (tractor_ns_3+'/odom')], shell=True),
    ])
