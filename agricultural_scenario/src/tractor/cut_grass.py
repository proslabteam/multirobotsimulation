#!/usr/bin/env python3

import rclpy
import time

from nav_msgs.msg import Odometry
from rclpy.node import Node
from geometry_msgs.msg import Wrench
from agriculture.msg import LogTopic
from rclpy.parameter import Parameter


class CutGrass():
    def __init__(self, node: Node):
        self.node = node
        self.blade_force = Wrench()
        self.removed = False
        self.destroy = False
        self.message = LogTopic()

        self.node.get_logger().info('--> BLADE node')

        self.macro_pub = self.node.create_publisher(LogTopic, 'macro', 10)
        self.odom_sub = self.node.create_subscription(
            Odometry, 'odom', self.check_pos, 10)
        self.blade_pub = self.node.create_publisher(Wrench, 'blade_force', 10)
        self.timer = self.node.create_timer(0.5, self.timer_callback)

        self.message.activity = 'CUT_GRASS'
        self.message.lifecycle = 'START'
        self.message.payload = self.node.get_namespace()
        self.macro_pub.publish(self.message)

        #self.node.get_logger().info('>>>>>>>>>>>>>>>>>>>>> Started')

    def timer_callback(self):
        self.blade_pub.publish(self.blade_force)

    def check_pos(self, odometry: Odometry):
        x = odometry.pose.pose.position.x
        y = odometry.pose.pose.position.y

        if not self.removed:
            self.blade_force.torque.z = 1.0
            self.removed = True
            self.node.get_logger().info('Weed removed at:  %d ; %d' % (x, y))
            #self.node.get_logger().info('>>> ' + str(self.node.get_parameter('busy').get_parameter_value().bool_value))
            time.sleep(0.5)
            self.blade_force.torque.z = 0.0
            self.destroy = True
            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            my_new_param = Parameter(
                    'busy',
                    rclpy.Parameter.Type.BOOL,
                    False
                )
            self.node.set_parameters([my_new_param])
            #self.node.get_logger().info('>>> ' + str(self.node.get_parameter('busy').get_parameter_value().bool_value))
            

