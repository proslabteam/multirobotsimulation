#!/usr/bin/env python3

import rclpy

from nav_msgs.msg import Odometry
from rclpy.parameter import Parameter
from std_msgs.msg import String
from rclpy.node import Node
from geometry_msgs.msg import Vector3, Vector3Stamped
from move_to import MoveTo
from agriculture.msg import LogTopic


class TractorController(Node):
    def __init__(self):
        super().__init__('tractor_controller')
        self.namespace = self.get_namespace()
        self.namespace = self.namespace.replace('/', '')
        self.vector_dist = Vector3Stamped()
        self.last_weed_msg = Vector3()
        self.my_position = Vector3()
        self.macro = LogTopic()
        self.weed_found = False
        self.itsMe = False
        self.pos_sent = False
        self.check_timer = 0

        self.declare_parameter('busy', False)
        self.busy = self.get_parameter('busy').get_parameter_value().bool_value

        self.create_subscription(
            String, 'low_battery', self.battery_handler, 10)

        self.receive_weed()

        rclpy.spin(self)

    def receive_weed(self):
        # SUB weed_position
        self.create_subscription(
            Vector3, '/weed_position', self.msg_checker, 10)
        self.create_subscription(Odometry, 'odom', self.get_position, 10)

        self.macro_pub = self.create_publisher(LogTopic, 'macro', 10)

        timer_period = 0.5
        self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        if self.weed_found:
            # PUB tractor_position
            current_position_pub = self.create_publisher(
                Vector3Stamped, '/tractor_position', 10)

            self.vector_dist.header.frame_id = self.namespace
            self.vector_dist.vector = self.my_position

            # Publish current position only once
            if not self.pos_sent:
                self.macro.activity = 'WEED_POSITION'
                self.macro.lifecycle = 'START'
                self.macro.payload = self.vector_to_string(
                    '', self.last_weed_msg)
                self.macro_pub.publish(self.macro)

                self.macro.lifecycle = 'STOP'
                self.macro_pub.publish(self.macro)

                self.macro.activity = 'TRACTOR_POSITION'
                self.macro.payload = self.vector_to_string(
                    self.vector_dist.header.frame_id, self.vector_dist.vector)
                self.macro.lifecycle = 'START'
                self.macro_pub.publish(self.macro)

                current_position_pub.publish(self.vector_dist)
                self.macro.lifecycle = 'STOP'
                self.macro_pub.publish(self.macro)
                self.pos_sent = True
                # print incoming weed (only once)
                self.get_logger().info('>> New incoming weed at (%d; %d) <<' %
                                       (self.last_weed_msg.x, self.last_weed_msg.y))

            # increment the timer after message publication
            self.check_timer += 0.5

            # SUB closest_tractor
            sub = self.create_subscription(
                String, '/closest_tractor', self.resp_wait, 10)

            # If the timer is elapsed and I haven't received a response
            if self.check_timer >= 30:
                self.weed_found = False
                self.get_logger().info('Timer is elapsed: %d!' % (self.check_timer))
                self.check_timer = 0
                self.destroy_subscription(sub)

    def vector_to_string(self, header: str, vect: Vector3):
        string = '{weed_x: ' + str(vect.x) + \
            '} {weed_y: ' + str(vect.y) + '}'

        if header != '':
            string = '{header: ' + header + '} ' + string
        return string

    def msg_checker(self, weed_vector: Vector3):
        self.busy = self.get_parameter('busy').get_parameter_value().bool_value
        # if it is not performing another removal
        if not self.busy:
            if self.last_weed_msg.x != weed_vector.x or self.last_weed_msg.y != weed_vector.y:
                self.last_weed_msg.x = weed_vector.x
                self.last_weed_msg.y = weed_vector.y

                self.weed_found = True
            else:
                self.weed_found = False

    def get_position(self, odometry: Odometry):
        self.my_position.x = odometry.pose.pose.position.x
        self.my_position.y = odometry.pose.pose.position.y

    def resp_wait(self, tractor: String):
        if self.pos_sent:
            if tractor.data == self.namespace:
                self.get_logger().info(self.namespace + ': IT IS ME!')
                self.itsMe = True
                self.weed_found = False
                my_new_param = Parameter(
                    'busy',
                    rclpy.Parameter.Type.BOOL,
                    True
                )
                self.set_parameters([my_new_param])
                self.busy = self.get_parameter(
                    'busy').get_parameter_value().bool_value
                # if I'm the closest tractor activate the MoveTo task/node
                MoveTo(self, vector=self.last_weed_msg)
                # self.get_logger().info('THISSSS')
            else:
                # if I'm not the closest tractor
                self.get_logger().info(self.namespace + ': IT IS NOT FOR ME!')
                self.itsMe = False
                self.weed_found = False

                my_new_param = Parameter(
                    'busy',
                    rclpy.Parameter.Type.BOOL,
                    False
                )
                self.set_parameters([my_new_param])
                self.busy = self.get_parameter(
                    'busy').get_parameter_value().bool_value

        self.pos_sent = False
        self.check_timer = 0

 # If the battery is too low
    def battery_handler(self, battery_msg: String):
        self.warning_msg = 'LOW BATTERY'
        if battery_msg.data == self.warning_msg:
            my_new_param = Parameter(
                'busy',
                rclpy.Parameter.Type.BOOL,
                True
            )
            self.set_parameters([my_new_param])
            self.busy = self.get_parameter(
                'busy').get_parameter_value().bool_value

            self.destroy_node()


def setup():
    rclpy.init()
    controller = TractorController()
    rclpy.spin(controller)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
