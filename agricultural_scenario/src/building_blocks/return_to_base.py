#!/usr/bin/env python3
from move_to import MoveTo
from rclpy.node import Node
from geometry_msgs.msg import Vector3
from agriculture.msg import LogTopic

class ReturnToBase():
    def __init__(self, node: Node):
        #x_dest = node.get_parameter('home_x').get_parameter_value().integer_value
        #y_dest = node.get_parameter('home_y').get_parameter_value().integer_value

        #node.get_logger().info("DEST %d %d" % (x_dest, y_dest))

        self.pub_macro = node.create_publisher(LogTopic, 'macro', 10)

        self.macro = LogTopic()
        vector = Vector3()
        str_appendix = node.get_namespace()
        if str_appendix.startswith('tractor_'):
            str_appendix = str_appendix.removeprefix('tractor_')
            vector.x = int(str_appendix)
        else:
            vector.x = 5.0
            vector.z = -1.0 #used to identify the drone
        vector.y = 0.0

        self.macro.activity = 'RETURN_TO_BASE'
        self.macro.lifecycle = 'START'
        self.pub_macro.publish(self.macro)

        self.macro.lifecycle = 'STOP'
        self.pub_macro.publish(self.macro)

        MoveTo(node, vector)
