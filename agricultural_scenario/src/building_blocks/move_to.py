#!/usr/bin/env python3

#import rclpy
import math
#import numpy as np
import time
from rclpy.node import Node
from geometry_msgs.msg import Vector3, Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Range
from tf_transformations import euler_from_quaternion
from cut_grass import CutGrass
from agriculture.msg import LogTopic
from flight_controller import flight_service


class MoveTo():
    def __init__(self, node: Node, vector: Vector3):
        self.destination = vector
        self.node = node

        if self.destination.z < 0.0:
            flight_service('land')
            node.destroy_node()
            
        self.velocity = Twist()
        self.reached = False
        self.range = Range()
        self.position = Odometry()
        self.macro_message = LogTopic()
        self.isRotating = False
        self.ang_vel = 0.5
        self.lin_vel = 0.5
        self.roll = self.pitch = self.yaw = 0.0
        self.obstacle = False

        #self.range_sub = self.create_subscription(Range, 'range', self.avoid_obstacles, 10)
        self.odom_sub = node.create_subscription(
            Odometry, 'odom', self.get_position, 10)
        self.velocity_pub = node.create_publisher(Twist, 'cmd_vel', 10)
        self.macro_topic_pub = node.create_publisher(LogTopic, 'macro', 10)

        timer_period = 0.5  # seconds
        self.timer = node.create_timer(timer_period, self.velocity_callback)

        self.macro_message.activity = 'MOVE'
        self.macro_message.lifecycle = 'START'

        self.macro_topic_pub.publish(self.macro_message)

    def destroy_activity(self):
        self.node.destroy_publisher(self.velocity_pub)
        self.node.destroy_subscription(self.odom_sub)

    def velocity_callback(self):  # publish on cmd_vel
        self.velocity_pub.publish(self.velocity)

    def rotate(self, angle):
        self.isRotating = True
        #target_rad = angle*math.pi/180
        check = angle-self.yaw
        self.velocity.angular.z = self.ang_vel * (check)

        if check < 0.002 and check > -0.002:
            self.isRotating = False

    # Obstacle avoidance
    def avoid_obstacles(self, range: Range):
        self.range = range.range
        if self.range < 0.5:
            self.rotate(1.57)
            self.velocity_pub.publish(self.velocity)

    def get_position(self, odometry: Odometry):
        # set current position
        self.position = odometry.pose.pose
        x = self.position.position.x
        y = self.position.position.y
        orientation_q = odometry.pose.pose.orientation
        orientation_list = [orientation_q.x, orientation_q.y,
                            orientation_q.z, orientation_q.w]
        # conversion to roll, pitch and yaw
        (self.roll, self.pitch, self.yaw) = euler_from_quaternion(orientation_list)

        diff_x = self.destination.x - x
        diff_y = self.destination.y - y

        angle_to_goal = math.atan2(diff_y, diff_x)

    # Check if the destination is achieved
        if abs(diff_x) < 0.65 and abs(diff_y) < 0.65:
            if not self.reached:
                self.velocity.linear.x = 0.0
                self.velocity.angular.z = 0.0
                self.velocity_pub.publish(self.velocity)

                self.macro_message.lifecycle = 'STOP'
                self.macro_topic_pub.publish(self.macro_message)

                time.sleep(0.2)

                # self.destroy_activity()
                self.reached = True

                self.node.get_logger().info(self.node.get_name())
                # avoid the cut grass action if the move to has been called by battery controller
                if self.node.get_name() == 'tractor_controller':
                    CutGrass(self.node)
                else:
                    self.node.destroy_node()
        else:
            self.rotate(angle_to_goal)
            if not self.isRotating:
                self.velocity.linear.x = self.lin_vel
                self.velocity.angular.z = 0.0

        self.velocity_pub.publish(self.velocity)
