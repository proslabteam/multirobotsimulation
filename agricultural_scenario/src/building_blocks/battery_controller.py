#!/usr/bin/env python3

from return_to_base import ReturnToBase
import rclpy
from rclpy.node import Node
import sys
from std_msgs.msg import String
from move_to import MoveTo
from agriculture.msg import LogTopic


class Battery(Node):
    def __init__(self, battery_percentage):
        self.warning_msg = String()
        self.message = LogTopic()
        self.warning_msg.data = 'LOW BATTERY'
        self.current_battery = float(battery_percentage)
        self.msg_sent = False

        # creates a node
        super().__init__('battery_controller')
        self.low_battery_pub = self.create_publisher(String, 'low_battery', 10)

        self.macro_topic_pub = self.create_publisher(LogTopic, 'macro', 10)

        #self.create_subscription(String, 'low_battery', self.battery_handler, 10)
        self.create_timer(5, self.battery_dec)

        rclpy.spin(self)

        if KeyboardInterrupt:
            self.destroy_node()
            rclpy.shutdown()


    def battery_dec(self):
        #self.get_logger().info(self.get_namespace() + ' --- ' + str(self.current_battery))
        self.current_battery -= 1.0

        if self.current_battery <= 5 and not self.msg_sent:
            self.message.activity = 'LOW_BATTERY'
            self.message.lifecycle = 'START'
            self.macro_topic_pub.publish(self.message)

            self.low_battery_pub.publish(self.warning_msg)
            self.get_logger().info('WARNING!! Battery level is under 5%')

            self.message.activity = 'LOW_BATTERY'
            self.message.lifecycle = 'STOP'
            self.macro_topic_pub.publish(self.message)

            self.msg_sent = True
            
            ReturnToBase(self)




def setup():
    rclpy.init()
    battery_node = Battery(sys.argv[1])
    rclpy.spin(battery_node)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
