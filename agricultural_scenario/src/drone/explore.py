#!/usr/bin/env python3
import rclpy
import math
import time
import os

from nav_msgs.msg import Odometry
from rclpy.node import Node
from rclpy.task import Future
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Range
from agriculture.msg import LogTopic
from std_msgs.msg import String
#from math import atan2
from tf_transformations import euler_from_quaternion
#from return_to_base import ReturnToBase


class Explore(Node):

    def __init__(self):
        self.velocity = Twist()
        self.curr_odometry = Odometry()
        self.status = ''
        self.map = [0, 9.5]  # 10m*10m of map
        self.weed_vector = Vector3()
        self.destroy = False
        self.weed_found = False
        self.message = LogTopic()
        self.message.activity = 'EXPLORE'
        self.clockwise = True

        super().__init__('explore')
        print('--> EXPLORE node')

        self.future_msg = Future()

        self.create_subscription(Odometry, 'odom', self.boundaries_check, 10)

        self.create_subscription(Range, 'range', self.find_weed, 10)

        self.create_subscription(
            String, '/closest_tractor', self.closest_callback, 10)

        self.velocity_pub = self.create_publisher(
            Twist, 'cmd_vel', 10)
        self.weed_pub = self.create_publisher(Vector3, 'weed_found', 10)

        self.macro_pub = self.create_publisher(LogTopic, 'macro', 10)

        self.timer = self.create_timer(0.5, self.timer_callback)

        self.message.lifecycle = 'START'
        self.macro_pub.publish(self.message)

        # self.future_msg.done()

        #rclpy.spin_until_future_complete(self, self.future_msg)
        rclpy.spin(self)

    def closest_callback(self, tractor_name: String):
        if tractor_name.data != None:
            self.destroy = False
            self.status = 'GO'
            self.message.lifecycle = 'START'
            self.macro_pub.publish(self.message)

    def timer_callback(self):
        if not self.destroy:
            self.velocity_pub.publish(self.velocity)

    def rotate(self, angle):
        if not self.clockwise:
            angle = - angle

        target_rad = angle*math.pi/180
        check = target_rad-self.yaw
        # self.get_logger().info(str(self.yaw))

        # self.get_logger().info(str(check))
        #self.get_logger().info(str(self.curr_odometry.pose.covariance))
        self.velocity.angular.z = 0.3
        self.velocity.linear.x = 0.2

        if check < 0.05 and check > -0.05:
            self.status = 'GO'
            self.velocity.linear.x = 0.6
            self.velocity.angular.z = 0.0
            self.clockwise = not self.clockwise

    def move_to(self, status: str):

        x = self.curr_odometry.pose.pose.position.x
        y = self.curr_odometry.pose.pose.position.y

        # if the drone identifies a weed
        if self.weed_found and not self.destroy:
            self.weed_vector.x = x
            self.weed_vector.y = y

            # PUB weed_position
            self.get_logger().info('Weed at: %d; %d' % (x, y))

            # activate the sub-process
            self.weed_pub.publish(self.weed_vector)

            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            self.velocity.linear.x = 0.0
            self.velocity.angular.z = 0.0

            self.velocity_pub.publish(self.velocity)

            time.sleep(0.2)

            # destroy Explore node
            self.destroy = True

            #self.future_msg._done = True

            # self.future_msg.done()

            # self.destroy_node()

            # self.destroy_callback()

        # if the drone is exploring
        if status == 'TURN':
            orientation_q = self.curr_odometry.pose.pose.orientation
            orientation_list = [orientation_q.x, orientation_q.y,
                                orientation_q.z, orientation_q.w]
            # conversion to roll, pitch and yaw
            (self.roll, self.pitch, self.yaw) = euler_from_quaternion(orientation_list)

            self.rotate(180)

        elif status == 'GO':
            self.velocity.linear.x = 0.6
            self.velocity.angular.z = 0.0

        #elif status == 'GO_HOME':
        #    ReturnToBase(self)

    # Check the navigation area

    def boundaries_check(self, odometry: Odometry):
        self.curr_odometry = odometry
        odom_x = odometry.pose.pose.position.x
        odom_y = odometry.pose.pose.position.y

        self.status = 'GO'

        #if odom_y > self.map[1] and odom_x > self.map[1]:
        #    self.status = 'GO_HOME'
        if not self.in_range(odom_x, self.map[0], self.map[1]):
            self.status = 'TURN'
        elif not self.in_range(odom_y, self.map[0], self.map[1]):
            self.status = 'TURN'

        # self.get_logger().info('STATUS ' + str(self.status))
        self.move_to(self.status)

    def in_range(self, i, goal_min, goal_max):
        self.range = 0.15
        goal_min = goal_min - 0.15
        goal_max = goal_max + 0.15

        if i >= goal_min and i <= goal_max:
            return True
        else:
            return False

    def timer_checker(self):
        if not self.weed_found:
            self.get_logger().info("Stop!")
            os.system("killall -9 gazebo & killall -9 gzserver  & killall -9 gzclient")

    def find_weed(self, range: Range):
        self.distance_range = range.range
        if self.distance_range < 1.4 and self.curr_odometry.pose.pose.position.z > 1.42:
            self.weed_found = True
            self.create_timer(300, self.timer_checker)
        else:
            self.weed_found = False


