#!/usr/bin/env python3
import os
from explore import Explore
import rclpy
import time
from nav_msgs.msg import Odometry
from tello_msgs.srv import TelloAction
from agriculture.msg import LogTopic

message = LogTopic()

# call ROS2 service
def flight_service(command: str):
    node = rclpy.create_node('flight_service')

    cli = node.create_client(TelloAction, 'tello_action')

    message.activity = str(command.upper())

    req = TelloAction.Request()
    req.cmd = command

    while not cli.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('service not available, waiting again...')

    macro_pub = node.create_publisher(LogTopic, 'macro', 10)

    future = cli.call_async(req)
    rclpy.spin_until_future_complete(node, future)

    message.lifecycle = 'START'
    macro_pub.publish(message)

    result = future.result()

    node.get_logger().info(str(result.rc))

    message.lifecycle = 'STOP'
    macro_pub.publish(message)

    time.sleep(0.2)

    if command == 'land':
        os.system("killall -9 gazebo & killall -9 gzserver  & killall -9 gzclient")

    node.destroy_node()


def altitude_status(odometry: Odometry):
    odom_z = odometry.pose.pose.position.z

    if(odom_z < 0.5):
        flight_service('takeoff')
        time.sleep(0.2)

    Explore()


def setup():
    rclpy.init()
    node = rclpy.create_node('flight_controller')

    node.create_subscription(Odometry, 'odom', altitude_status, 10)

    rclpy.spin(node)

    if KeyboardInterrupt:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
