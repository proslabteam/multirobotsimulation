#!/usr/bin/env python3

from explore import Explore
import math
import rclpy

from rclpy.node import Node
from std_msgs.msg import String
from geometry_msgs.msg import Vector3, Vector3Stamped
from agriculture.msg import LogTopic


weed_found_msg = 'WEED_FOUND'
weed_pos_msg = 'WEED_POSITION'
tractor_msg = 'TRACTOR_POSITION'
closest_msg = 'CLOSEST_TRACTOR'


class WeedSubProcess(Node):
    def __init__(self):
        super().__init__('weed_node')

        self.weed = Vector3()
        self.tractor = String()
        self.message = LogTopic()

        self.received_msgs = []
        self.timer = 0
        self.max_time = 10

        self.closest_tractor = False
        self.weed_found = False
        self.resend = False

        self.create_subscription(Vector3, 'weed_found', self.get_weed, 10)

        self.create_subscription(
            Vector3Stamped, '/tractor_position', self.tractor_msgs, 10)

        self.weed_pub = self.create_publisher(Vector3, '/weed_position', 10)

        self.macro_pub = self.create_publisher(LogTopic, 'macro', 10)

        self.tractor_pub = self.create_publisher(
            String, '/closest_tractor', 10)

        self.create_timer(0.5, self.weed_callback)
        self.create_timer(0.5, self.tractor_callback)
        #self.create_timer(10, self.print)

        rclpy.spin(self)

    # def print(self):
    #    self.get_logger().info("PRINT")

    def vector_to_string(self, header: str, vect: Vector3):
        string = '{weed_x: ' + str(vect.x) + \
            '} {weed_y: ' + str(vect.y) + '}'

        if header != '':
            string = '{header: ' + header + '} ' + string
        return string

    # Sub-process that manages tractor responses

    def tractor_msgs(self, vector: Vector3Stamped):
        if not vector in self.received_msgs:
            self.received_msgs.append(vector)
            self.message.activity = tractor_msg
            self.message.lifecycle = 'START'
            self.message.payload = self.vector_to_string(
                vector.header.frame_id, vector.vector)
            self.macro_pub.publish(self.message)

            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            self.message.payload = ''

    def check_responses(self):
        responses = []

        # if 30 seconds are elapsed and there are no responses
        if len(self.received_msgs) == 0:
            print('No responses received!')
            self.closest_tractor = False
            self.resend = True

        # if the drone has received only one response
        elif len(self.received_msgs) == 1:
            self.tractor.data = self.received_msgs[0].header.frame_id
            self.closest_tractor = True
            self.resend = False

        else:
            for msg in self.received_msgs:
                vect = msg.vector
                header = msg.header.frame_id
                distance = math.sqrt(
                    math.pow((vect.x - self.weed.x), 2) + math.pow((vect.y - self.weed.y), 2))
                if len(responses) > 1:
                    # if the distance is lower than a previous one
                    if responses[0] > distance and not len(responses[1]) == 0:
                        responses.clear()
                        responses.append(distance)
                        responses.append(header)
                # if there are no elements
                else:
                    responses.append(distance)
                    responses.append(header)

            self.tractor.data = responses[1]
            self.closest_tractor = True
            self.resend = False

    def get_weed(self, vector: Vector3):
        self.weed.x = vector.x
        self.weed.y = vector.y
        self.get_logger().info('>>>>>>>>>>>>> WEED <<<<<<<<<<<<<<<<<<<<')

        self.weed_found = True

    def weed_callback(self):
       # publish weed coordinates only once
        self.weed_sent = False

        if (not self.weed_sent and self.weed_found) or self.resend:
            self.message.activity = weed_found_msg
            self.message.lifecycle = 'START'
            self.macro_pub.publish(self.message)

            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            self.weed_pub.publish(self.weed)
            self.timer += 0.5
            self.weed_sent = True
            self.weed_found = False

            self.message.activity = weed_pos_msg
            self.message.lifecycle = 'START'
            self.message.payload = self.vector_to_string('', self.weed)
            self.macro_pub.publish(self.message)

            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            self.message.payload = ''

        # increase the timer for 10 seconds
        if self.timer > 0 and self.timer < self.max_time:
            self.timer += 0.5
        # if 10 sec has been passed, the responses will be evaluated
        elif self.timer >= self.max_time:
            self.timer = 0
            self.check_responses()

    def tractor_callback(self):
        if self.closest_tractor:
            self.message.activity = closest_msg
            self.message.lifecycle = 'START'
            self.message.payload = 'name: ' + str(self.tractor.data)
            self.macro_pub.publish(self.message)

            self.tractor_pub.publish(self.tractor)
            self.get_logger().info('-------> ' + self.tractor.data)

            self.message.lifecycle = 'STOP'
            self.macro_pub.publish(self.message)

            self.closest_tractor = False

            self.received_msgs.clear()

            self.message.payload = ''

           # self.get_logger().info("1: __________________________")

           # Explore()

           # self.get_logger().info("2: __________________________")


def setup():
    rclpy.init()
    weed_node = WeedSubProcess()
    rclpy.spin(weed_node)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
