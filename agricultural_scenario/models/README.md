# Models
Add these folders in:

```
 ~/.gazebo/models
```

## Folder organization

```  
  .
  ├── models                        
  |   ├── model_name               
  |   │   ├── materials
  |   |   |   ├── scripts       
  |   │   |   ├── textures      
  |   |   ├── model.config
  └───└───└── model.sdf
```
