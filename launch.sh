#!/bin/sh
cd ~/dev_ws

colcon build

if ! command -v xterm
then
    sudo apt-get update -y
    sudo apt-get install -y xterm
else
    echo "xterm installed"
fi

xterm -e bash -c 'ros2 launch agriculture multi_launch.py' &

xterm -e bash -c 'sleep 10; ros2 launch agriculture controllers.launch.py' &

sleep 15;

while true
do
if pgrep -x "gzserver" > /dev/null
then
    sleep 0.5
else
    echo "Stopped"
    killall xterm
    break
fi 
done


cd ~/dev_ws/logged_files

python converter.py

python merge.py

cd ..
