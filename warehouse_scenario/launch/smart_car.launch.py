
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    ns_1 = 'car_1'
    ns_2 = 'car_2'

    return LaunchDescription([

        Node(package='warehouse', executable='car_controller.py', output='screen',
             namespace=ns_1),
        Node(package='warehouse', executable='car_controller.py', output='screen',
             namespace=ns_2),

    ])
