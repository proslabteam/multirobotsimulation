import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch_ros.actions import Node


def generate_launch_description():
    # Get the launch directory
    aws_small_warehouse_dir = get_package_share_directory(
        'warehouse')
    forklift = os.path.join(aws_small_warehouse_dir,
                            'robot_description', 'forklift_1.sdf')
    forklift2 = os.path.join(aws_small_warehouse_dir,
                             'robot_description', 'forklift_2.sdf')
    small_robot = os.path.join(aws_small_warehouse_dir,
                               'robot_description', 'small_robot_1.sdf')
    small_robot2 = os.path.join(aws_small_warehouse_dir,
                                'robot_description', 'small_robot_2.sdf')

    # Launch configuration variables specific to simulation
    use_sim_time = LaunchConfiguration('use_sim_time')
    use_simulator = LaunchConfiguration('use_simulator')
    headless = LaunchConfiguration('headless')
    world = LaunchConfiguration('world')

    declare_use_sim_time_cmd = DeclareLaunchArgument(
        'use_sim_time',
        default_value='True',
        description='Use simulation (Gazebo) clock if true')

    declare_simulator_cmd = DeclareLaunchArgument(
        'headless',
        default_value='False',
        description='Whether to execute gzclient)')

    declare_world_cmd = DeclareLaunchArgument(
        'world',
        default_value=os.path.join(
            aws_small_warehouse_dir, 'worlds', 'simple_warehouse.world'),
        description='Full path to world model file to load')

    # Specify the actions
    start_gazebo_server_cmd = ExecuteProcess(
        cmd=['gzserver', '--verbose', '-s', 'libgazebo_ros_init.so',
             '-s', 'libgazebo_ros_factory.so', world],
        cwd=[aws_small_warehouse_dir], output='screen')

    start_gazebo_client_cmd = ExecuteProcess(
        condition=IfCondition(PythonExpression(['not ', headless])),
        cmd=['gzclient'],
        cwd=[aws_small_warehouse_dir], output='screen')

    # Create the launch description and populate
    ld = LaunchDescription([
        # Spawn robots.urdf
        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[forklift, '6.4', '4.2', '0', '-1.54', 'forklift_1']),

        # Spawn robots.urdf
        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[forklift2, '6.4', '9.5', '0', '-1.54', 'forklift_2']),

        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[small_robot, '0.5', '0.5', '0', '0', 'car_1']),
        Node(package='agriculture', executable='spawn_elements.py', output='screen',
             arguments=[small_robot2, '0.5', '0.3', '0', '0', 'car_2']),

        # Publish static transforms
        Node(package='robot_state_publisher', executable='robot_state_publisher', output='screen',
             arguments=[forklift]),

        Node(package='robot_state_publisher', executable='robot_state_publisher', output='screen',
                     arguments=[forklift2]),
        Node(package='robot_state_publisher', executable='robot_state_publisher', output='screen',
             arguments=[small_robot]),

    ])
    # Declare the launch options
    ld.add_action(declare_use_sim_time_cmd)
    ld.add_action(declare_simulator_cmd)
    ld.add_action(declare_world_cmd)

    # Add any conditioned actions
    ld.add_action(start_gazebo_server_cmd)
    ld.add_action(start_gazebo_client_cmd)

    return ld
