#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Twist
from std_msgs.msg import Bool, String
from sensor_msgs.msg import Range
from math import atan2, sqrt, pow

# box_size = [0.2, 0.5, 0.4]
boxes_pos = {
    'b1': [2.0, 0.5],
    'b2': [2.0, 2.3],
    'b3': [2.0, 4.1],
    'b4': [2.0, 5.9],
    'b5': [2.0, 7.7],
    'b6': [4.0, 1.5],
    'b7': [4.0, 3.3],
    'b8': [4.0, 5.1],
    'b9': [4.0, 6.9],
    'b10': [4.0, 8.7]
}

base = {'car_1': [0.5, 0.5], 'car_2': [0.5, 0.3]}
position = Odometry()
velocity = Twist()
namespace = ''
no_area = False
no_boxes = False
car_list = []
reached = Bool()
busy = False
range_dist = 0.0


class ReturnToBase(Node):
    def __init__(self):
        super().__init__('return_to_base')
        self.x = base[namespace][0]
        self.y = base[namespace][1]

        self.create_subscription(Odometry, 'odom', get_position, 10)

        self.vel_pub = self.create_publisher(Twist, 'cmd_vel', 10)
        self.create_timer(0.5, self.velocity_callback)
        rclpy.spin(self)

    def velocity_callback(self):
        self.get_logger().info(str(position.pose))
        go_to(self.x, self.y)
        self.vel_pub.publish(velocity)


class MoveToBox(Node):
    def __init__(self):
        super().__init__('move_to_box')
        self.velocity = Twist()
        self.destination_box = Point()
        self.free = True
        self.box = False

        self.destination_pub = self.create_publisher(
            Point, '/incoming_box', 10)

        self.vel_pub = self.create_publisher(Twist, 'cmd_vel', 10)
        self.reached_pub = self.create_publisher(Bool, '/car_ready', 10)

        self.create_subscription(Odometry, 'odom', get_position, 10)

        self.create_timer(0.5, self.load_callback)
        self.create_timer(0.5, self.velocity_callback)

        rclpy.spin(self)

    def velocity_callback(self):
       # self.get_logger().info(str(range_dist))
        if self.box:
            go_to(self.destination_box.x, self.destination_box.y)
            self.vel_pub.publish(velocity)
            if reached.data:
                self.get_logger().info('Arrived')
                self.reached_pub.publish(reached)
                reached.data = False
                self.destroy_node()

    def load_callback(self):
        if self.free:
            self.closest_box(position, boxes_pos)
            self.get_logger().info("New BOX")
            self.destination_pub.publish(self.destination_box)
            self.free = False
            self.box = True

    def closest_box(self, pos: Odometry, boxes: dict):
        my_x = pos.pose.pose.position.x
        my_y = pos.pose.pose.position.y

        distance = 100.0
        key = ''
        if len(boxes.keys()) > 0:
            for b in boxes.keys():
                x = boxes[b][0]
                y = boxes[b][1]
                distance_temp = calculate_dist(my_x, my_y, x, y)
                if distance_temp < distance:
                    distance = distance_temp
                    key = b
        else:
            global no_boxes
            no_boxes = True

        self.destination_box.x = boxes_pos.get(key)[0]
        self.destination_box.y = boxes_pos.get(key)[1]

        boxes_pos.pop(key)

        if namespace.endswith('2'):
            self.destination_box.y -= 0.1
        elif namespace.endswith('1'):
            self.destination_box.y += 0.1


class PushBox(Node):
    def __init__(self):
        super().__init__('push_box')
        self.velocity = Twist()

        self.create_subscription(str, '/car_stop', self.push, 10)
        self.velocity_pub = self.create_publisher(Twist, 'cmd_vel', 10)

    def push(self, str_msg: String):
        if str_msg.data != 'BoxOK':
            self.velocity.linear.x = 0.2
            self.velocity.angular.z = 0.0
        else:
            self.velocity.linear.x = 0.0
            self.velocity.angular.z = 0.0
        self.velocity_pub.publish(self.velocity)


def go_to(x_dest, y_dest):
    global reached
    x = position.pose.pose.position.x
    y = position.pose.pose.position.y

    q = position.pose.pose.orientation

    siny_cosp = 2 * (q.w * q.z + q.x * q.y)
    cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)

    yaw = atan2(siny_cosp, cosy_cosp)

    diff_x = x_dest - x
    diff_y = y_dest - y

    angle_to_goal = atan2(diff_y, diff_x)

    if range_dist < 0.35:
        velocity.linear.x = 0.0
        velocity.angular.z = 0.0
        reached.data = True
    else:
        # Destination reached
        if abs(angle_to_goal - yaw) <= 0.1 and inRange(x, x_dest) and inRange(y, y_dest):
            velocity.linear.x = 0.0
            velocity.angular.z = 0.0
            reached.data = True
        elif abs(angle_to_goal - yaw) > 0.1:
            velocity.linear.x = 0.0
            velocity.angular.z = 0.2
        else:
            velocity.linear.x = 0.2
            velocity.angular.z = 0.0


def inRange(i, goal):
    range = 0.15
    min = goal - range
    max = goal + range

    if i >= min and i <= max:
        return True
    else:
        return False


def calculate_dist(x, y, x1, y1):
    distance = sqrt(pow((x - x1), 2) + pow((y - y1), 2))
    return distance


def get_position(odometry: Odometry):
    global position
    position = odometry


def no_free_area(status: Bool):
    global no_area
    no_area = status.data


def car_ready(string: String):
    global car_list
    if string.data not in car_list:
        car_list.append(string.data)


def current_range(range: Range):
    global range_dist
    range_dist = range.range


def setup():
    rclpy.init()
    node = rclpy.create_node('car_controller')

    global namespace, car_list
    namespace = node.get_namespace()
    # remove the slash to use the namespace for values checking
    namespace = namespace.replace('/', '')

    node.create_subscription(Bool, '/no_free_area', no_free_area, 10)
    node.create_subscription(String, '/car_ready', car_ready, 10)
    node.create_subscription(Range, 'range', current_range, 10)

    def timer_callback():
        global busy
        if not busy:
            if no_area or no_boxes:
                rb_node = ReturnToBase()
                rclpy.spin(rb_node)
            else:
                move = MoveToBox()
                rclpy.spin(move)
                busy = True

        if len(car_list) == 2:
            node.get_logger().info('READY TO PUSH')
            car_list.clear()
            push = PushBox()
            rclpy.spin(push)

    node.create_timer(0.5, timer_callback)
    rclpy.spin(node)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
