#!/usr/bin/env python3

from warehouse_scenario.src.industrial_car.car_controller import go_to
import rclpy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Twist
from sensor_msgs.msg import Range
from std_msgs.msg import String, Bool
from rclpy.node import Node
from math import atan2, sqrt

free_areas = {
    'a1': [9.5, 0.5],
    'a2': [9.5, 1.5],
    'a3': [9.5, 2.5],
    'a4': [9.5, 3.5],
    'a5': [9.5, 4.5],
    'a6': [9.5, 5.5],
    'a7': [9.5, 6.5],
    'a8': [9.5, 7.5],
    'a9': [9.5, 8.5],
    'a10': [9.5, 9.5]}

box_pos = Point()
busy = False
load = False


class MoveTo(Node):
    def __init__(self):
        self.velocity = Twist()
        self.position = Odometry()

        self.reached = False

        # creates a node
        super().__init__('move_to')

        self.create_subscription(Odometry, 'odom', self.get_position, 10)

        self.velocity_pub = self.create_publisher(
            Twist, 'cmd_vel', 10)

        self.timer = self.create_timer(0.5, self.timer_callback)

        rclpy.spin(self)

        if KeyboardInterrupt or self.reached:
            self.destroy_timer(self.timer)
            self.destroy_node()
            rclpy.shutdown()

    def timer_callback(self):
        if not self.reached:
            self.load_box(box_pos)
            self.velocity_pub.publish(self.velocity)

    def get_position(self, odom: Odometry):
        self.position = odom

    def load_box(self, point: Point):
        self.x = self.position.pose.pose.position.x
        self.y = self.position.pose.pose.position.y

        self.q = self.position.pose.pose.orientation

        self.siny_cosp = 2 * (self.q.w * self.q.z + self.q.x * self.q.y)
        self.cosy_cosp = 1 - 2 * (self.q.y * self.q.y + self.q.z * self.q.z)

        self.yaw = atan2(self.siny_cosp, self.cosy_cosp)

        self.y_dest = abs(self.y - point.y)

        if not self.inRange(self.yaw, -1.54):
            self.velocity.angular.z = -0.2
        elif self.y_dest > 1.0:
            self.velocity.linear.x = 0.5
            self.velocity.angular.z = 0.0
            self.reached = True
        else:
            self.velocity.linear.x = 0.0
            self.velocity.angular.z = 0.0

    def inRange(self, i, goal):
        range = 0.15
        min = goal - range
        max = goal + range

        if i >= min and i <= max:
            return True
        else:
            return False


class FillFreeArea(Node):
    def __init__(self):
        self.velocity = Twist()
        self.position = Odometry()
        self.area = Point()

        # creates a node
        super().__init__('free_area')

        self.create_subscription(Odometry, 'odom', self.get_position, 10)
        self.closest_area(self.position, free_areas)

        self.create_timer(0.5, self.goto_callback)
        self.vel_pub = self.create_publisher(Twist, 'cmd_vel', 10)

    def goto_callback(self):
        self.go_to(self.velocity, self.area.x, self.area.y)
        self.vel_pub.publish(self.velocity)

    def get_position(self, odom: Odometry):
        self.position = odom

    def closest_area(self, pos: Odometry, free_areas: dict):
        my_x = pos.pose.pose.position.x
        my_y = pos.pose.pose.position.y

        distance = 100.0
        key = ''
        if len(free_areas.keys()) > 0:
            for a in free_areas.keys():
                x = free_areas[a][0]
                y = free_areas[a][1]
                distance_temp = sqrt(pow((x - my_x), 2) + pow((y - my_y), 2))
                if distance_temp < distance:
                    distance = distance_temp
                    key = a
        else:
            self.destroy_node()

        self.area.x = free_areas.get(key)[0]
        self.area.y = free_areas.get(key)[1]

        free_areas.pop(key)

    def go_to(self, velocity: Twist, x_dest, y_dest):
        x = self.position.pose.pose.position.x
        y = self.position.pose.pose.position.y

        q = self.position.pose.pose.orientation

        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)

        yaw = atan2(siny_cosp, cosy_cosp)

        diff_x = x_dest - x
        diff_y = y_dest - y

        angle_to_goal = atan2(diff_y, diff_x)

        if range_dist < 0.35:
            velocity.linear.x = 0.0
            velocity.angular.z = 0.0
        else:
            # Destination reached
            if abs(angle_to_goal - yaw) <= 0.1 and self.inRange(x, x_dest) and self.inRange(y, y_dest):
                velocity.linear.x = 0.0
                velocity.angular.z = 0.0
            elif abs(angle_to_goal - yaw) > 0.1:
                velocity.linear.x = 0.0
                velocity.angular.z = 0.2
            else:
                velocity.linear.x = 0.2
                velocity.angular.z = 0.0

    def inRange(self, i, goal):
        range = 0.15
        min = goal - range
        max = goal + range

        if i >= min and i <= max:
            return True
        else:
            return False


def box(box_coordinates: Point):
    global box_pos
    box_pos = box_coordinates


def range_dist(r: Range):
    global load
    # if the perceived range is lower than 0.3, the forklift has a loaded box
    if r.range < 0.3:
        load = True
    else:
        load = False


def setup():
    rclpy.init()
    node = rclpy.create_node('controller')
    stop_pub = node.create_publisher(String, '/car_stop', 10)
    area_pub = node.create_publisher(Bool, '/no_free_area', 10)

    def timer_callback():
        # advertize the cars that the area slots are all full
        if len(free_areas.keys()) == 0:
            no_area_msg = Bool()
            no_area_msg.data = True
            area_pub.publish(no_area_msg)

        global busy
        if not busy:
            node.create_subscription(Point, '/incoming_box', box, 10)
            busy = True
            MoveTo()
        else:
            node.create_subscription(Range, 'range', range_dist, 10)
            if load:
                str_msg = String()
                str_msg.data = 'BoxOK'
                node.get_logger().info('BOX loaded!')
                stop_pub.publish(str_msg)

    node.create_timer(0.5, timer_callback)

    rclpy.spin(node)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
