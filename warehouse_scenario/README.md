# Multi-Robot application in a Warehouse Scenario

## Description
The proposed application scenario consists of two industrial cars that collaborate in order to push some heavy boxes, while cooperating with two forklifts that uploads these packs and organize them into a loading area.
The cars know the boxes position, and as long as there are some, they synchronously move to the nearest one and start pushing. When they have reached a box an advertising message is sent to the forklifts that are triggered.
As soon as a forklift has reached the loading area, the industrial cars pushes until the box is loaded into it. At this point the forklift is able to reach a free area, whereas the cars restarts their collaboration


## BPMN model

![BPMN model](./docs/bpmn_models/warehouse.png)

