# Gazebo simulation of MRS with ROS2

## Installation

### Requirements

* Ubuntu 20.04 Focal Fossa
* ROS2 Foxy Fitzroy
* Gazebo simulator

### Manual installation:

```bash
mkdir -p ~/dev_ws/
cd ~/dev_ws/
git clone https://bitbucket.org/proslabteam/multirobotsimulation.git
mv -v multirobotsimulation src
cd ~/dev_ws/
rosdep install --from-paths src --ignore-src -r -y
sudo apt install libasio-dev
colcon build
source install/setup.bash

cp -R ~/dev_ws/src/agricultural_scenario/models ~/.gazebo
cp -R ~/dev_ws/src/warehouse_scenario/models ~/.gazebo
cp -R ~/dev_ws/src/city_scenario/models ~/.gazebo

echo 'source ~/dev_ws/install/setup.bash' >> ~/.bashrc 
```

### Auto installation:

* Download the source file [here](./installation.sh)
* Run:
```bash
source install.sh
```
------

## Agricultural Scenario 🥕

**Full details [here](./agricultural_scenario/README.md)**

### Run simulation

*From launch file*:
```bash
ros2 launch agriculture multi_launch.py
```


*From script*:
```bash
cd ~/dev_ws/src
source agri_launch.sh
```

![Agri](img/agriculture.jpg)


### Credits
The tello_ros pakage is from: https://github.com/clydemcqueen/tello_ros

------

## Factory warehouse 🏭

**Full details [here](./warehouse_scenario/README.md)**


### Run simulation

*From launch file*:
```bash
ros2 launch warehouse multi_robot_warehouse_launch.py
```


*From script*:
```bash
cd ~/dev_ws/src
source warehouse_launch.sh
```

![Warehouse](img/warehouse.jpg)

------

## City Surveillance 🏠

**Full details [here](./city_scenario/README.md)**

### Run simulation

*From launch file*:
```bash
ros2 launch city multi_robot_city.launch.py
```


*From script*:
```bash
cd ~/dev_ws/src
source city_launch.sh
```

### Credits 
World file: https://github.com/chapulina/dolly/blob/foxy/dolly_gazebo/worlds/dolly_city.world

![City](img/city.jpg)


------
