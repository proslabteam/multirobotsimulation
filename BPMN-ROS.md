# BPMN - ROS mapping

| BPMN elements | ROS code |
| :------------- |:-------------:|
|![Element: Init](img/bpmn_elements/init.png)| **rclpy**.init() **rclpy**.create_node|
|![Element: Call Activity](img/bpmn_elements/call_activity.png)| **class** ClassName(**Node**)|
|![Element: Script Task](img/bpmn_elements/script_task.png)| **def** method(_param_)|
|![Element: Data Object](img/bpmn_elements/data_obj.png)| global_variable = _value_|
|![Element: Signal Catching](img/bpmn_elements/signal_catch.png)| **node**.create_subscription(...)|
|![Element: Signal Throwing](img/bpmn_elements/signal_throw.png)| **node**.create_publisher(...)|
|![Element: Timer](img/bpmn_elements/signal_timer.png)| **node**.create_timer(...)|
|![Element: Terminate](img/bpmn_elements/stop.png)| **node**.destroy_node()   **rclpy**.shutdown()|
