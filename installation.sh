#!/bin/bash

mkdir -p ~/dev_ws/
cd ~/dev_ws/
git clone https://bitbucket.org/proslabteam/multirobotsimulation.git
mv -v multirobotsimulation src
cd ~/dev_ws/
rosdep install --from-paths src --ignore-src -r -y
sudo apt install libasio-dev
colcon build
source install/setup.bash
echo 'source ~/dev_ws/install/setup.bash' >> ~/.bashrc 

cp -R ~/dev_ws/src/agricultural_scenario/models ~/.gazebo
cp -R ~/dev_ws/src/warehouse_scenario/models ~/.gazebo
cp -R ~/dev_ws/src/city_scenario/models ~/.gazebo
