# Multi-Robot application in a City Surveillance Scenario

## Description
The proposed application scenario consists of 9 drones that scan the city in a coordinating way, in order to monitor it.
Looking at the hardware side, the drones are homogeneous, they are made up of four propellers and equipped with a camera and laser sensor, to better analyze the environment.
At the start-up, each drone knows the size of the city area. The drone with the higher battery level is chosen as leader. Therefore, the leader chooses 6 drones to be active, each of which is sent  to a city zone that has to be monitored.
In the meantime the drones in the landing area waits until something happens. If one of the operating drone needs to come back to the landing area or fails, another one goes and covers its area. Whereas, if a drone perceive some problems in the city, it triggers those ones that are not performing the surveillance, which will act as reinforcements.



## BPMN model

![BPMN model](./docs/bpmn_models/surveillance.png)

### Call Activities
| Name | Model |
| :------------- |:-------------:|
| Decide Active Drones | <img src="./docs/bpmn_models/Decide_active_drones.png" width="600" />|
| Monitor Designed Area | <img src="./docs/bpmn_models/Monitor_designed_area.png" width="600" /> |
| Rescure | <img src="./docs/bpmn_models/Rescure.png" width="600" /> |