#!/usr/bin/env python3
import rclpy

from nav_msgs.msg import Odometry
from rclpy.node import Node
from geometry_msgs.msg import Twist, Vector3
from math import atan2


class ReturnToBase(Node):
    def __init__(self):
        self.home = [0, 0]
        self.reached = False
        self.velocity = Twist()
        self.goal = Vector3()
        self.goal.x = 0.0
        self.goal.y = 0.0
        super().__init__('return_to_base')
        print('--> RETURN node')

        self.create_subscription(Odometry, 'odom', self.go_home, 10)
        self.velocity_pub = self.create_publisher(
            Twist, 'cmd_vel', 10)
        self.timer = self.create_timer(0.5, self.timer_callback)

        rclpy.spin(self)

        if KeyboardInterrupt or self.reached:
            self.destroy_timer(self.timer)
            self.destroy_node()
            rclpy.shutdown()

    def timer_callback(self):
        self.velocity_pub.publish(self.velocity)
        if self.reached:
            print('land')
            #service('land')

    def go_home(self, odometry : Odometry):
        x = odometry.pose.pose.position.x
        y = odometry.pose.pose.position.y
        q = odometry.pose.pose.orientation

        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)

        yaw = atan2(siny_cosp, cosy_cosp)

        diff_x = self.goal.x - x
        diff_y = self.goal.y - y

        angle_to_goal = atan2(diff_y, diff_x)

        if abs(angle_to_goal - yaw) > 0.1:
            if not self.inRange(x, self.goal.x) and not self.inRange(y, self.goal.y):
                self.velocity.linear.x = 0.0
                self.velocity.angular.z = 0.2
            else:
                self.velocity.linear.x = 0.0
                self.velocity.angular.z = 0.5

        # Destination reached
        elif abs(angle_to_goal - yaw) <= 0.1 and self.inRange(x, self.goal.x) and self.inRange(y, self.goal.y):
            self.velocity.linear.x = 0.0
            self.velocity.angular.z = 0.0
            print('ARRIVED!!!!')
            self.reached = True
        else:
            self.velocity.linear.x = 0.4
            self.velocity.angular.z = 0.0

    def inRange(self, i, goal):
        self.range = 0.15
        self.min = goal - self.range
        self.max = goal + self.range

        if i >= self.min and i <= self.max:
            return True
        else:
            return False


def setup():
    rclpy.init()
    return_node = ReturnToBase()
    rclpy.spin(return_node)

    if KeyboardInterrupt:
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
