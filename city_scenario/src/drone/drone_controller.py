#!/usr/bin/env python3

from os import name
import rclpy
import time
from rclpy.node import Node
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from sensor_msgs.msg import BatteryState
from std_msgs.msg import String, Bool
from tello_msgs.srv import TelloAction


leader = ''
initial_position = Point()
landed = True
active = Bool()
map = [
    [123, 48.5],
    [123, -48.5],
    [-48.5, -48.5],
    [-48.5, 48.5]
]
drones_batteries = {}


class LeaderElection(Node):
    def __init__(self):
        super().__init__('leader_election')
        self.lower_battery = BatteryState()

        self.create_subscription(BatteryState, '/battery', self.elect, 10)

        self.leader_pub = self.create_publisher(String, '/leader', 10)

        rclpy.spin(self)

        self.destroy_node()

    def elect(self, battery: BatteryState):
        self.get_logger().info(str(drones_batteries.keys()))
        if battery.header.frame_id not in drones_batteries.keys():
            drones_batteries.update({battery.header.frame_id : battery.voltage})
            if self.lower_battery.header.frame_id == '':
                self.lower_battery = battery
            else:
                if battery.voltage < self.lower_battery.voltage:
                    self.lower_battery = battery
                else:
                    return
        self.get_logger().info(self.lower_battery.header.frame_id)

        if len(drones_batteries.keys()) == 9:
            self.leader_pub.publish(self.lower_battery.header.frame_id)
            self.get_logger().info(self.lower_battery.header.frame_id)


class DecideActiveDrones(Node):
    def __init__(self):
        super().__init__('active_drones')

        sort_drones = sorted(drones_batteries.items(), key=lambda x: x[1], reverse=True)

        # sort by higher battery values
        for i in range(6):
            drone_ns = sort_drones[i][0]
            pub = self.create_publisher(Bool, drone_ns + '/active', 10)
            self.send_active_mgs(pub)
            i +=1

        rclpy.spin(self)

        self.destroy_node()

    def send_active_mgs(self, publisher):
        msg = Bool()
        msg.data = True
        publisher.publish(msg)


class MonitorArea(Node):
    def __init__(self):
        super().__init__('monitor')

        rclpy.spin(self)

        self.destroy_node()


def altitude_status(odometry: Odometry):
    odom_x = odometry.pose.pose.position.x
    odom_y = odometry.pose.pose.position.y
    odom_z = odometry.pose.pose.position.z

    global initial_position, landed

    if(odom_z < 0.5) and landed:
        initial_position.x = odom_x
        initial_position.y = odom_y
        service('takeoff')
        landed = False
        time.sleep(0.2)


# call ROS2 service
def service(command: str):
    node = rclpy.create_node('flight_service')
    cli = node.create_client(TelloAction, 'tello_action')

    req = TelloAction.Request()
    req.cmd = command

    while not cli.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('service not available, waiting again...')

    future = cli.call_async(req)
    rclpy.spin_until_future_complete(node, future)

    result = future.result()
    node.get_logger().info(str(result.rc))

    node.destroy_node()


def am_I_leader(str: String):
    global leader
    leader = str.data


def active_status(status: Bool):
    global active
    active = status.data


def setup():
    rclpy.init()
    node = rclpy.create_node('drone_controller')
    namespace = node.get_namespace()
    namespace = namespace.replace('/', '')

    node.create_subscription(Odometry, 'odom', altitude_status, 10)

    LeaderElection()

    node.create_subscription(String, '/leader', am_I_leader, 10)
    node.create_subscription(Bool, 'active', active_status, 10)

    def timer_callback():
        if leader == namespace:
            DecideActiveDrones()
        elif active:
            MonitorArea()
        else:
            return

    node.create_timer(60, timer_callback)

    rclpy.spin(node)

    if KeyboardInterrupt:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    setup()
