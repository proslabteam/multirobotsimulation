#!/usr/bin/env python3

import sys
import rclpy
import time
from std_msgs.msg import String
from sensor_msgs.msg import BatteryState
from return_to_base import ReturnToBase



initial_battery = 8000.0  # Wh
current_battery = initial_battery
warning_msg = String()
warning_msg.data = 'LOW BATTERY'

def battery_handler(msg : String):
    if msg.data == warning_msg.data:
        ReturnToBase()

def setup(init_battery):
    rclpy.init()
    node = rclpy.create_node('battery_controller')
    print('--> BATTERY node')

    global initial_battery
    initial_battery = init_battery

    low_battery_pub = node.create_publisher(String, 'low_battery', 10)
    battery_pub = node.create_publisher(BatteryState, '/battery', 10)
    node.create_subscription(String, 'low_battery', battery_handler, 10)

    def battery_dec():
        global current_battery

        b = BatteryState()
        b.header.frame_id = node.get_namespace()
        b.voltage = current_battery
        battery_pub.publish(b)

        current_battery -= 1.5

        if current_battery <= 5:
            low_battery_pub.publish(warning_msg)
            node.get_logger().info('WARNING!! Battery level is under 5 Wh')

    node.create_timer(60, battery_dec)

    rclpy.spin(node)

    if KeyboardInterrupt:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    setup(sys.argv[1])
