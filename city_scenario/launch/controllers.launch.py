from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    ld = LaunchDescription()

    for index in range(9):
        drone_ns = 'tello_'+str(index+1)
        index += 1

        battery = 8000 + index

        node = Node(package='city', executable='drone_controller.py', output='screen',
                    namespace=drone_ns)

        batt_node = Node(package='city', executable='battery_controller.py', output='screen', arguments=[str(battery)],
                    namespace=drone_ns)

        ld.add_action(node)
        ld.add_action(batt_node)


    return ld
