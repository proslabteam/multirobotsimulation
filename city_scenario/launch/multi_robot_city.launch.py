import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess


def generate_launch_description():

    pkg = get_package_share_directory('city')

    world_path = os.path.join(pkg, 'worlds', 'custom_city.world')

    gazebo_process = ExecuteProcess(cmd=[
        'gazebo',
        '--verbose',
        '-s', 'libgazebo_ros_init.so',  # Publish /clock
        '-s', 'libgazebo_ros_factory.so',  # Provide gazebo_ros::Node
        world_path
    ], output='screen')

    ld = LaunchDescription()

    ld.add_action(gazebo_process)

    initX = 22.0
    initY = -38.0
    counterX = 0
    counterY = 0

    for index in range(9):
        counterY += 3
        if index == 3 or index == 6:
            counterX += 3
            counterY = 3

        tello_ns = 'tello_'+str(index+1)

        tello_pkg = os.path.join(
            pkg, 'robot_description/', str(tello_ns + '.sdf'))

        tempX = initX + counterX
        tempY = initY + counterY

        x = str(tempX)
        y = str(tempY)

        node = Node(package='agriculture', executable='spawn_elements.py', output='screen',
                    arguments=[tello_pkg, x, y, '0', '0', tello_ns])

        rsp = Node(package='robot_state_publisher', executable='robot_state_publisher', output='screen',
                   arguments=[tello_pkg])


        ld.add_action(node)
        ld.add_action(rsp)

    return ld
