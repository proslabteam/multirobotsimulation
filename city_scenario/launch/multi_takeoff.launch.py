import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from rclpy import init


def generate_launch_description():

    pkg = get_package_share_directory('robot_description')

    ld = LaunchDescription()

    for index in range(10):
        tello_ns = 'tello_'+str(index+1)

        node = Node(package='robot_description', executable='service.py', output='screen',
             arguments=['takeoff', tello_ns]),

        ld.add_action(node)

    return ld
