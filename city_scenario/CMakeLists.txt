cmake_minimum_required(VERSION 3.5)

project(city)

# Skip if Gazebo not present
find_package(gazebo QUIET)
if(NOT gazebo_FOUND)
  message(WARNING "Gazebo not found, proceeding without that simulator.")
  return()
endif()

find_package(ament_cmake REQUIRED)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()
endif()

file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/robot_description")

foreach (INDEX RANGE 0 11)
  if (${INDEX} EQUAL 0)
    set(SUFFIX "")
    set(TOPIC_NS "tello")
  else ()
    set(SUFFIX "_${INDEX}")
    set(TOPIC_NS "tello${INDEX}")
  endif ()
  set(URDF_FILE "${CMAKE_CURRENT_BINARY_DIR}/robot_description/tello${SUFFIX}.sdf")
  message(STATUS "creating rules for ${URDF_FILE}")
  add_custom_command(
    OUTPUT ${URDF_FILE}
    COMMAND ${PYTHON_EXECUTABLE} "${CMAKE_CURRENT_SOURCE_DIR}/src/replace.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/robot_description/tello.sdf" "suffix=${SUFFIX}" "topic_ns=${TOPIC_NS}" ">" "${URDF_FILE}"
    DEPENDS robot_description/tello.sdf
    COMMENT "Generate ${URDF_FILE}"
    VERBATIM
  )
  add_custom_target(generate_tello_urdf${SUFFIX} ALL DEPENDS ${URDF_FILE})
endforeach ()

install(
  DIRECTORY
    launch
    models
    "${CMAKE_CURRENT_BINARY_DIR}/robot_description"
    worlds
  DESTINATION
    share/${PROJECT_NAME}/
)

install(
  PROGRAMS
    launch/controllers.launch.py
    src/drone/drone_controller.py
    src/drone/battery_controller.py
    src/drone/return_to_base.py
  DESTINATION lib/${PROJECT_NAME}
)

ament_package()
